import numpy as np
import cv2
import base64
import uuid
import paho.mqtt.client as paho
import time
from multiprocessing import Process
import os
import numpy as np
import datetime
import urllib3
import json
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import as_completed

video_capture = cv2.VideoCapture(0)
video_capture.set(3, 320)
video_capture.set(4, 240)

url_base='http://localhost:5000/api'
http = urllib3.PoolManager()

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))    

client = paho.Client()
client.on_message = on_message
client.connect("nadirloutfi.hopto.org", 1883)

def split_every(chunk, buffer):
    return [ buffer[i:i+chunk] for i in range(0, len(buffer), chunk) ]

def get_image_base64(image):
    retval, buf = cv2.imencode('.png', image)
    png_as_text = base64.b64encode(buf)
    return png_as_text

def centered_crop(img, new_width, new_height):
    
   width =  np.size(img,1)
   height =  np.size(img,0)

   left = int(np.ceil((width - new_width)/2.))
   top = int(np.ceil((height - new_height)/2.))
   right = int(width - np.floor((width - new_width) / 2))
   bottom = int(np.floor((height + new_height)/2.))
   cimg = img[top:bottom, left:right]
   return cimg, (left, top), (right, bottom)

def publish_image(folder_name, name, croped_image):
    image_as_text = get_image_base64(croped_image)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())

    http.request(
        'POST',
        "{}/publish/{}/{}".format(url_base, folder_name, name),
                headers={'Content-Type': 'application/json'},
                 body=json.dumps({'image': image_as_text.decode("utf-8")}))
    
    time.sleep(1)

def draw_text(frame, text, x, y, fontscale=2):
    if x is not None and y is not None:
        cv2.putText(
            frame, text, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX,fontscale,(196,117,86),2,cv2.LINE_AA)

def get_center(p1, p2):
    return (p1[0] + (p2[0] - p1[0])/2, p1[1] + (p2[1] - p1[1])/2)


def predict(croped_image):
    image_as_text = get_image_base64(croped_image)
    res = http.request(
        'POST',
        "{}/classify".format(url_base),
                headers={'Content-Type': 'application/json'},
                 body=json.dumps({'image': image_as_text.decode("utf-8")}))
    json_result = res.data.decode('utf8').replace("'", '"')
    return json.loads(json_result)

last_command=None
start_count_down = False
target = None
MAX_COUNT_DOW = 9
MAX_SNAPSHOT = 10
count_down = MAX_COUNT_DOW
name = None
snapshot = 0
SIZE_WIDTH=0.30
SIZE_HEIGHT=0.40

while(True):
    # Capture frame-by-frame
    ok, frame = video_capture.read()
    if not ok:
        break;

    frame = cv2.flip( frame, 1 )

    # draw a rectangle around the region of interest
    img_height, img_width, _ = frame.shape
    snap_height = img_height - (img_height - (img_height * SIZE_HEIGHT))
    snap_width  = img_width -  (img_width - (img_width * SIZE_WIDTH))
    croped_image, snap_left_top, snap_right_bottom = centered_crop(frame, snap_width, snap_height)
    cv2.rectangle(frame, snap_left_top, snap_right_bottom, (0, 255, 0), 2)

    key = cv2.waitKey(1) & 0xFF
    
    if last_command == None and key != 0xFF:
        last_command = key

    if last_command == ord('s'):
        # start countdown
        if not start_count_down or name is None or len(name) == 0:
            name = input("Please enter your name: ")
            res = http.request('POST', "{}/user/{}".format(url_base, name))
            json_result = res.data.decode('utf8').replace("'", '"')
            data = json.loads(json_result)
            if data['result'] is False:
                print("Name {} already exists.".format(name))
            else:
                target = datetime.datetime.now()
                start_count_down = True
        else:
            target += datetime.timedelta(seconds=1)
            elapsed_time = (target - datetime.datetime.now()).total_seconds()
            if elapsed_time > 1 and count_down > 0:
                center = get_center(snap_left_top, snap_right_bottom)
                draw_text(frame, str(count_down), center[0], center[1])
                count_down -= 1
                time.sleep(.200)
                print(count_down, 'remaining', end='\r')
            elif count_down <= 0:
                if snapshot < MAX_SNAPSHOT:
                    p = Process(target=publish_image, args=(name, "{}_{}".format(name, snapshot), croped_image,))
                    p.start()
                    p.join()
                    snapshot += 1
                    draw_text(frame, "Photo {}".format(snapshot), center[0], center[1], 1)
                    time.sleep(1)
                else:
                    last_command = None
                    start_count_down = False
                    count_down = MAX_COUNT_DOW
                    name = None
                    snapshot = 0

    if last_command == ord('c'):
        res = predict(croped_image)        
        if len(res['confidences']) and res['confidences'][0] > 0.4:
            person = res['persons'][0]
        else:
            person = 'Unknown'
        draw_text(frame, person, snap_left_top[0], snap_left_top[1], 1)
        print(res)
        # with ProcessPoolExecutor(max_workers=1) as executor:
        #     results = executor.map(predict, croped_image, timeout=60)
        #     for result in results:
        #         print(result)

    # Display the resulting frame
    cv2.imshow('frame',frame)

    if key == ord('q'):
        break

# When everything done, release the capture
video_capture.release()
cv2.destroyAllWindows()