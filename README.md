This POC is based on OpenFace 
* http://cmusatyalab.github.io/openface/setup
* http://bamos.github.io/2016/01/19/openface-0.2.0/

OpenFace is a Python and Torch implementation of face recognition with deep neural networks and is based on the CVPR 2015 paper FaceNet: 
A Unified Embedding for Face Recognition and Clustering by Florian Schroff, Dmitry Kalenichenko, and James Philbin at Google. 
Torch allows the network to be executed on a CPU or with CUDA.

Crafted by Brandon Amos, Bartosz Ludwiczuk, and Mahadev Satyanarayanan.


**Setup POC:**
1. git clone git@gitlab.com:XXXXX/facerecognition.git
2. cd YOURWORKSPACE/facerecognition
3. docker-compose up

**Train only**

docker-compose up train

**Run service REST**

docker-compose up web

**The volume mapping is not working with docker-compose, so the workaround is to use docker with mapping volume in its parameters**

**Train only**

*docker run -v $(pwd):/root/openface/facerecognition -p 5000:5000 -it IMAGE_ID /bin/sh -c 'cd /root/openface/facerecognition; ./generate_model.sh'*

**Run service REST**

*docker run -v $(pwd):/root/openface/facerecognition -p 5000:5000 -it IMAGE_ID /bin/sh -c 'cd /root/openface/facerecognition; pip install -r requirements.txt; python classifier_service.py'*

**Take snapshot**

When the video is on, you can press 's', set your name in the prompt. see the camera and wait the snapshot finishing.

**Face recognition**

When the video is on, you can press 'c', to recognize the face.


**Test publish**
Parameter image in Base64

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://localhost:5000/api/publish/titi/titi1",
  "method": "POST",
  "headers": {
    "Content-Type": "application/json"
  },
  "data": "{\"image\": \"iVBORw0KGgoAAAANSUhEUgAAAYAAAAFQCAIAAA....."}
$.ajax(settings).done(function (response) {
  console.log(response);
}); 

**Test classify**

Parameter image in Base64

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://localhost:5000/api/classify",
  "method": "POST",
  "headers": {
    "Content-Type": "application/json",
    "cache-control": "no-cache",
    "Postman-Token": "83ee50c4-df88-4844-8ee7-63126e54164a"
  },
  "processData": false,
  "data": "{\n\t\"image\":\"iy+LfwJc2DoeHoTcAgAAAABJRU5ErkJggg==......."}"
}

$.ajax(settings).done(function (response) {
  console.log(response);
});

**Results**

See the results of applied images, according to the images defined in apply_images folder

=== ./apply_images/camel-ouali.jpg ===
Predict Nadir_Loutfi with 0.53 confidence.

=== ./apply_images/dt1.png ===
Predict Donald_Trump with 0.76 confidence.

=== ./apply_images/dt2.png ===
Predict Donald_Trump with 0.87 confidence.

=== ./apply_images/dt3.png ===
Predict Donald_Trump with 0.80 confidence.

=== ./apply_images/dt4.png ===
Predict Donald_Trump with 0.87 confidence.

=== ./apply_images/dt5.png ===
Predict Donald_Trump with 0.88 confidence.

=== ./apply_images/dt6.png ===
Predict Donald_Trump with 0.82 confidence.

=== ./apply_images/dt7.png ===
Predict Donald_Trump with 0.85 confidence.

=== ./apply_images/em2.png ===
Predict Emanuel_Macron with 0.84 confidence.

=== ./apply_images/em3.png ===
Predict Emanuel_Macron with 0.60 confidence.

=== ./apply_images/em4.png ===
Predict Emanuel_Macron with 0.87 confidence.

=== ./apply_images/em5.png ===
Predict Emanuel_Macron with 0.89 confidence.

=== ./apply_images/em7.png ===
Predict Emanuel_Macron with 0.83 confidence.

=== ./apply_images/fh1.png ===
Predict Francois_Holland with 0.80 confidence.

=== ./apply_images/fh2.png ===
Predict Francois_Holland with 0.84 confidence.

=== ./apply_images/fh3.png ===
Predict Francois_Holland with 0.89 confidence.

=== ./apply_images/fh4.png ===
Predict Francois_Holland with 0.72 confidence.

=== ./apply_images/fh5.png ===
Predict Francois_Holland with 0.86 confidence.

=== ./apply_images/fh6.png ===
Predict Francois_Holland with 0.91 confidence.

=== ./apply_images/fh7.png ===
Predict Francois_Holland with 0.93 confidence.

=== ./apply_images/nadir1.png ===
Predict Nadir_Loutfi with 0.87 confidence.

=== ./apply_images/nadir2.png ===
Predict Nadir_Loutfi with 0.83 confidence.

=== ./apply_images/nadir3.png ===
Predict Nadir_Loutfi with 0.81 confidence.

=== ./apply_images/nadirsanslunettes.png ===
Predict Nadir_Loutfi with 0.76 confidence.

=== ./apply_images/nalou.jpg ===
Predict Nadir_Loutfi with 0.79 confidence.


Citations

B. Amos, B. Ludwiczuk, M. Satyanarayanan,
"Openface: A general-purpose face recognition library with mobile applications,"
CMU-CS-16-118, CMU School of Computer Science, Tech. Rep., 2016.
