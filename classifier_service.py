import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../..', 'openface'))
import openface

import time
import json
import requests
import uuid
import base64

import cv2
import pickle

from flask import Flask, request, jsonify
app = Flask(__name__)

import numpy as np
np.set_printoptions(precision=2)
from sklearn.mixture import GMM

fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, '../', 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

networkModel = os.path.join(
                openfaceModelDir,
            'nn4.small2.v1.t7')
dlibFacePredictor = os.path.join(
                dlibModelDir,
                "shape_predictor_68_face_landmarks.dat")
imgDim = 96
classifierModel = './features/classifier.pkl'
align = openface.AlignDlib(dlibFacePredictor)
net = openface.TorchNeuralNet(
    networkModel,
    imgDim=imgDim,
    cuda=False)

def base64_to_img(encoded_data):
    decode_img = base64.b64decode(encoded_data)
    nparr = np.fromstring(decode_img, np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return img


def getFaces(bgrImg):
    if bgrImg is None:
        raise Exception("Unable to load image/frame")
    
    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

    # Get the largest face bounding box
    # bb = align.getLargestFaceBoundingBox(rgbImg) #Bounding box

    # Get all bounding boxes
    bb = align.getAllFaceBoundingBoxes(rgbImg)

    if bb is None:
        return None

    alignedFaces = []
    for box in bb:
        alignedFaces.append(
            align.align(
                imgDim,
                rgbImg,
                box,
                landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE))

    if alignedFaces is None:
        raise Exception("Unable to align the frame")

    faces = []
    for alignedFace in alignedFaces:
        faces.append(net.forward(alignedFace))

    return faces


def infer(image):
    result = {}
    with open(classifierModel, 'r') as f:
        (le, clf) = pickle.load(f)  # le - label and clf - classifer

    faces = getFaces(image)
    persons = []
    confidences = []
    for face in faces:
        try:
            face = face.reshape(1, -1)
        except:
            print ("No Face detected")
            return None
        predictions = clf.predict_proba(face).ravel()
        maxI = np.argmax(predictions)
        persons.append(le.inverse_transform(maxI))
        confidences.append(predictions[maxI])
        if isinstance(clf, GMM):
            dist = np.linalg.norm(face - clf.means_[maxI])
            print("  + Distance from the mean: {}".format(dist))
    result["persons"] = persons
    result["confidences"] = confidences
    print (result)
    return json.dumps(result)


def create_folder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
            print ('Creating directory. ' +  directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


@app.route('/ping')
def ping():
    return 'Pong'

@app.route('/api/classify', methods=['POST'])
def classify():
    content = request.json
    base64 = content['image']
    img = base64_to_img(base64)    
    return infer(img)

@app.route('/api/user/<name>', methods=['POST'])
def create_user(name):
    folder = "./images/{}".format(name)
    if os.path.exists(folder):
        message = "Name {} already exists.".format(name)
        result = False
    else:
        create_folder(folder)
        message = "OK"
        result = True
    return json.dumps({"result":result, "message": message})

@app.route('/api/publish/<folder_name>/<name>', methods=['POST'])
def publish(folder_name, name):
    content = request.json
    base64 = content['image']
    img = base64_to_img(base64)
    folder = "./images/{}".format(folder_name)
    create_folder(folder)
    cv2.imwrite("{}/{}.png".format(folder, name), img)
    return json.dumps({"result":"OK", "message": ""})

if __name__ == "__main__":
    print ("Server running on port 5000...")
    app.run(host='0.0.0.0', port=5000, debug=True)
