rm -R aligned_data
rm -R features
#apt-get update
#apt-get install -y mosquitto mosquitto-clients
MQTT_BROKER=nadirloutfi.fun
mosquitto_pub -t /dshop/connected_door/door1/reco/out -h $MQTT_BROKER -m '{"deviceid":"door1", "status":"training"}'
#Preprocess the raw images
../util/align-dlib.py /root/openface/facerecognition/images align outerEyesAndNose /root/openface/facerecognition/aligned_data --size 96

#Generate Representations
../batch-represent/main.lua -outDir ./features -data ./aligned_data

#Create the Classification Model
../demos/classifier.py train ./features

#Classifying New Images
../demos/classifier.py infer ./features/classifier.pkl ./apply_images/*

mosquitto_pub -t /dshop/connected_door/door1/reco/out -h $MQTT_BROKER -m '{"deviceid":"door1", "status":"idle"}'
