//
//  Classifier.swift
//  FaceUnlock
//
//  Created by LOUTFI, Nadir on 12/02/2019.
//  Copyright © 2019 LOUTFI, Nadir. All rights reserved.
//

import UIKit

class Classifier: NSObject {

    struct Static {
        static var SCALE_IN_PRECENT = 10 as CGFloat
    }

    var classify : ((UIImage?, (Data?, URLResponse?, Error?)) -> Void)?
    var settings = Settings()
    
    override init() {
        super.init()
        registerDefaultsFromSettingsBundle()
    }

}

extension Classifier {

    func registerDefaultsFromSettingsBundle()
    {
        let settingsUrl = Bundle.main.url(forResource: "Settings", withExtension: "bundle")!.appendingPathComponent("Root.plist")
        let settingsPlist = NSDictionary(contentsOf:settingsUrl)!
        let preferences = settingsPlist["PreferenceSpecifiers"] as! [NSDictionary]
        
        var defaultsToRegister = Dictionary<String, Any>()
        
        for preference in preferences {
            guard let key = preference["Key"] as? String else {
                NSLog("Key not fount")
                continue
            }
            defaultsToRegister[key] = preference["DefaultValue"]
        }
        UserDefaults.standard.register(defaults: defaultsToRegister)
    }

    func classify(image: UIImage, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        self.settings.read()
        
        if let imagebase64 = image.base64{
            if let url = URL(string: "\(self.settings.recognitionServer!)/api/classify") {
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                let data = ["image" : imagebase64]
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        completion(data, response, error)
                    }
                    task.resume()
                } catch let error as NSError {
                    print(error)
                }
            }
        }
    }

    func publish(folder: String, name: String, image: UIImage, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        self.settings.read()
        
        if let imagebase64 = image.base64{
            if let url = URL(string: "\(self.settings.recognitionServer!)/api/publish/\(folder)/\(name)") {
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                let data = ["image" : imagebase64]
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        completion(data, response, error)
                    }
                    task.resume()
                } catch let error as NSError {
                    print(error)
                }
            }
        }
    }
    
    public func randomNumber<T : SignedInteger>(inRange range: ClosedRange<T> = 1...6) -> T {
        let length = Int64(range.upperBound - range.lowerBound + 1)
        let value = Int64(arc4random()) % length + Int64(range.lowerBound)
        return T(value)
    }
}
