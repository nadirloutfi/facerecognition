
import UIKit
import Foundation

class ImageClassificationViewController: UIViewController {
    
    
    @IBOutlet weak var lock: UIButton!
    @IBOutlet weak var unlock: UIButton!
    // MARK: - IBOutlets
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var classificationLabel: UILabel!
    @IBOutlet fileprivate var captureButton: UIButton!
    @IBOutlet fileprivate var capturePreviewView: UIView!
    
    let cameraController = CameraController()
    
    let classifier = Classifier()
    
    let faceDetector = FaceDetector()
    
    let settings = Settings()

    var mqttClient: MQTTClient!
    
    struct Topic {
        static var DOOR = "door1"
        static var ROOT_BASE = "/dshop/connected_door/\(DOOR)"
        static var DOOR_COMMAND = "\(ROOT_BASE)/command/out"
        static var DOOR_RECOGNITION = "\(ROOT_BASE)/reco/out"
        static var COMMAND_OPEN = "open"
        static var COMMAND_CLOSE = "close"
        static var RECO_STATUS_IDLE = "idle"
        static var RECO_STATUS_SCAN = "scanning"
        static var RECO_STATUS_APPROVED = "approved"
        static var RECO_STATUS_REJECTED = "rejected"
    }

    var reco_processing = false
    
    @IBOutlet weak var activityView: InstagramActivityIndicator!
    var timer: Timer?
    
    func runTimer() -> Timer {
        timer = Timer.scheduledTimer(withTimeInterval: 10,
                                     repeats: true) {
                                        timer in
                                        self.classificationLabel.text = "Touch to unlock"
                                        self.mqttClient.publish(data: self.getRecoStatusPayload(status: Topic.RECO_STATUS_IDLE), topic: Topic.DOOR_RECOGNITION)
        }
        return timer!
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    

    override func viewDidAppear(_ animated: Bool) {
        timer = self.runTimer()
    }
    
    @IBAction func onTapGesture(_ sender: Any) {
        if (!reco_processing) {
            classify()
        }
    }
    
    
    func convertToDictionary(from text: String) -> NSDictionary? {
        var dictonary:NSDictionary?
        
        if let data = text.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                
            } catch let error as NSError {
                print(error)
            }
        }
        return dictonary
    }
    
    
    func getCommandPayload(command: String) -> Data {
        let json = ["deviceid" : Topic.DOOR, "command": command]
        return try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
    }

    func getRecoStatusPayload(status: String) -> Data {
        let json = ["deviceid" : Topic.DOOR, "status": status]
        return try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
    }

    
    func reject() {
        self.activityView.stopAnimating()
        reco_processing = false
        print("Unable to parse JSON response")
        self.mqttClient.publish(data: self.getRecoStatusPayload(status: Topic.RECO_STATUS_REJECTED), topic: Topic.DOOR_RECOGNITION)
    }
    
    func classify(for image: UIImage) {
        classificationLabel.text = "Predicting..."
        self.mqttClient.publish(data: self.getRecoStatusPayload(status: Topic.RECO_STATUS_SCAN), topic: Topic.DOOR_RECOGNITION)
        reco_processing = true
        DispatchQueue.global(qos: .userInitiated).async {
            //self.activityView.startAnimating()
            if (self.faceDetector.detected(image: image)) {
                let scaledImage = image.scaleInPercentImage(toPercent: Classifier.Static.SCALE_IN_PRECENT)
                self.classifier.classify(image: scaledImage, completion: { (data: Data?, response: URLResponse?, error: Error?) in
                    if data != nil {
                        do {
                            let resultObject = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String:Any]
                            DispatchQueue.main.async(execute: {
                                if ((resultObject["persons"] as! [Any]).count > 0) {
                                    let confidence = ((resultObject["confidences"] as! [Any]).first as! NSNumber).floatValue
                                    print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                                    if (confidence > self.classifier.settings.minConfidence) {
                                        self.classificationLabel.text =
                                            String(format: "  (%.2f) %@", confidence, (resultObject["persons"] as! [Any]).first as! CVarArg)
                                        self.mqttClient.publish(data: self.getRecoStatusPayload(status: Topic.RECO_STATUS_APPROVED), topic: Topic.DOOR_RECOGNITION)
                                        self.mqttClient.publish(data: self.getCommandPayload(command: Topic.COMMAND_OPEN), topic: Topic.DOOR_COMMAND)
                                    } else{
                                        self.classificationLabel.text = "Unknown"

                                        self.mqttClient.publish(data: self.getRecoStatusPayload(status: Topic.RECO_STATUS_REJECTED), topic: Topic.DOOR_RECOGNITION)

                                        self.mqttClient.publish(data: self.getCommandPayload(command: Topic.COMMAND_CLOSE), topic: Topic.DOOR_COMMAND)
                                    }
                                } else {
                                    self.classificationLabel.text = "Unknown"
                                    self.reject()
                                }
                                self.reco_processing = false
                                self.activityView.stopAnimating()
                            })
                        } catch {
                            DispatchQueue.main.async(execute: {
                                self.reject()
                            })
                        }
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.reject()
                        })
                    }
                })}
        }
    }
    
    func classify() {
        cameraController.captureImage {(image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            self.classify(for: image)
        }
    }

    @IBAction func cameraFlip(_ sender: Any) {
        do {
            try cameraController.switchCameras()
        } catch let error as NSError {
            print(error)
        }
    }

}


extension ImageClassificationViewController {

    override func viewDidLoad() {
        self.activityView.stopAnimating()
        self.activityView.hidesWhenStopped = true
        func configureCameraController() {
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
                
                try? self.cameraController.displayPreview(on: self.capturePreviewView)
            }
        }
        
        configureCameraController()
        
        self.settings.read()
        mqttClient = MQTTClient(host: self.settings.mqttServer!)
    }
}

