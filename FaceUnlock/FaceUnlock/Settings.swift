//
//  Settings.swift
//  FaceUnlock
//
//  Created by LOUTFI, Nadir on 16/02/2019.
//  Copyright © 2019 LOUTFI, Nadir. All rights reserved.
//

import Foundation

class Settings: NSObject {
    
    var mqttServer: String!
    var recognitionServer: String!
    var minConfidence: Float!
    
    struct SettingsBundleKeys {
        static let MqttServer = "mqtt_server"
        static let RecognitionServer = "recognition_server"
        static let MinConfidence = "min_confidence"
    }
    
    func read() {
        self.mqttServer = UserDefaults.standard.string(forKey: SettingsBundleKeys.MqttServer)
        self.recognitionServer = UserDefaults.standard.string(forKey: SettingsBundleKeys.RecognitionServer)
        self.minConfidence = UserDefaults.standard.float(forKey: SettingsBundleKeys.MinConfidence)
        
    }
}
