//
//  UIImage_Extension.swift
//  FaceMe
//
//  Created by LOUTFI, Nadir on 17/06/2017.
//  Copyright © 2017 SAP PA. All rights reserved.
//

import UIKit
extension UIImage {
    class func rad(degrees: CGFloat) -> CGFloat {return degrees * CGFloat(Double.pi / 180)}
    
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        var rectTransform:CGAffineTransform!
        switch (self.imageOrientation) {
        case UIImage.Orientation.left:
            let rotate = CGAffineTransform(rotationAngle: UIImage.rad(degrees: 90))
            rectTransform = rotate.translatedBy(x: 0, y: -self.size.height)
            break;
        case UIImage.Orientation.right:
            let rotate = CGAffineTransform(rotationAngle: UIImage.rad(degrees: -90))
            rectTransform = rotate.translatedBy(x: -self.size.width, y: 0)
            break;
        case UIImage.Orientation.down:
            let rotate = CGAffineTransform(rotationAngle: UIImage.rad(degrees: -180))
            rectTransform = rotate.translatedBy(x: -self.size.width, y:-self.size.height)
            break;
        default:
            rectTransform = CGAffineTransform.identity;
        };
        
        rectTransform = rectTransform.scaledBy(x: self.scale, y: self.scale)
        let imageRef = self.cgImage!.cropping(to: rect.applying(rectTransform))
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        
        
        return image
    }
    class func image(from layer: CALayer) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(layer.bounds.size,
                                               layer.isOpaque, UIScreen.main.scale)
        
        // Don't proceed unless we have context
        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func rotate(byDegrees degree: Double) -> UIImage? {
        let radians = CGFloat(degree*Double.pi)/180.0 as CGFloat
        let rotatedSize = self.size
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, self.scale)
        let bitmap = UIGraphicsGetCurrentContext()
        
        bitmap?.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        bitmap?.rotate(by: radians)
        bitmap?.scaleBy(x: 1, y: -1)
        bitmap?.draw(self.cgImage!, in:  CGRect(x: -self.size.width / 2, y: -self.size.height / 2 , width: self.size.width, height: self.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        return newImage
    }
    
    func base64EncodedString() -> String! {
        if let imageData:NSData = self.pngData() as NSData? {
            let strBase64 = imageData.base64EncodedString(options: .init(rawValue: 0))
            return strBase64
        }
        return nil
    }
    
    var base64: String? {
        guard let imageData = self.pngData() as NSData? else {
            print("Error occured while encoding image to base64. In \(self), \(#function)")
            return nil
        }
        return imageData.base64EncodedString()
    }
    
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        draw(in: CGRect(x:0, y:0, width: self.size.width, height: self.size.height))
        let normalizedImage:UIImage = (UIGraphicsGetImageFromCurrentImageContext())!
        UIGraphicsEndImageContext()
        
        return normalizedImage;
    }
        
    func flipImage() -> UIImage {
        guard let cgImage = self.cgImage else {
            // Could not form CGImage from UIImage for some reason.
            // Return unflipped image
            return self
        }
        let flippedImage = UIImage(cgImage: cgImage,
                                   scale: self.scale,
                                   orientation: .leftMirrored)
        return flippedImage
    }
    
    func scaleInPercentImage(toPercent: CGFloat) -> UIImage {
        
        let width = (self.size.width * toPercent)/100
        let height = (self.size.height * toPercent)/100
        return self.scaleImage(toSize: CGSize(width: width, height:height))!
    }
    
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        var newImage: UIImage?
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext(), let cgImage = self.cgImage {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(cgImage, in: newRect)
            if let img = context.makeImage() {
                newImage = UIImage(cgImage: img)
            }
            UIGraphicsEndImageContext()
        }
        return newImage
    }
    
    func cropped(boundingBox: CGRect) -> UIImage? {
        guard let cgImage = self.cgImage?.cropping(to: boundingBox) else {
            return nil
        }
        
        return UIImage(cgImage: cgImage)
    }
    
    
    func cropImage(toRect cropRect: CGRect, viewWidth: CGFloat, viewHeight: CGFloat) -> UIImage?
    {
        let imageViewScale = max(self.size.width / viewWidth,
                                 self.size.height / viewHeight)
        
        // Scale cropRect to handle images larger than shown-on-screen size
        let cropZone = CGRect(x:cropRect.origin.x * imageViewScale,
                              y:cropRect.origin.y * imageViewScale,
                              width:cropRect.size.width * imageViewScale,
                              height:cropRect.size.height * imageViewScale)
        
        // Perform cropping in Core Graphics
        guard let cutImageRef: CGImage = self.cgImage?.cropping(to:cropZone)
            else {
                return nil
        }
        
        // Return image to UIImage
        let croppedImage: UIImage = UIImage(cgImage: cutImageRef)
        return croppedImage
    }
}
