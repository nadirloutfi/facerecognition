//
//  ImageCaptureViewController.swift
//  FaceUnlock
//
//  Created by LOUTFI, Nadir on 12/02/2019.
//  Copyright © 2019 LOUTFI, Nadir. All rights reserved.
//

import Foundation
import UIKit

class ImageCaptureViewController: UIViewController,UINavigationControllerDelegate,  UIImagePickerControllerDelegate {
    
    @IBOutlet weak var takeImage: UIImageView!
    var imagePicker: UIImagePickerController!
    
    @IBOutlet weak var pseudoLabel: UILabel!
    
    @IBOutlet weak var activityView: InstagramActivityIndicator!
    let classifier = Classifier()
    
    let faceDetector = FaceDetector()

    
    @IBAction func takePhoto(_ sender: UIButton) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.actionTapped(_:)))
        pseudoLabel?.isUserInteractionEnabled = true
        pseudoLabel?.addGestureRecognizer(tapAction)
        self.activityView.hidesWhenStopped = true
        self.activityView.stopAnimating()
    }
    
    @objc func actionTapped(_ sender: UITapGestureRecognizer) {
        var alertViewControllerTextField: UITextField?
        let promptController = UIAlertController(title: "Name", message: "Enter your pseudo.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            self.pseudoLabel.text = alertViewControllerTextField?.text
            print("\(String(describing: alertViewControllerTextField?.text))")
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
        }
        promptController.addAction(ok)
        promptController.addAction(cancel)
        promptController.addTextField { (textField) -> Void in
            alertViewControllerTextField = textField
        }
        present(promptController, animated: true, completion: nil)
    }
    
    
    @IBAction func savePhoto(_ sender: UIButton) {
        //        UIImageWriteToSavedPhotosAlbum(takeImage.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        if (self.pseudoLabel.text != "NoName") {
            //self.activityView.startAnimating()
            if (faceDetector.detected(image: takeImage.image!)) {
                
                let scaledImage = takeImage.image!.scaleInPercentImage(toPercent: Classifier.Static.SCALE_IN_PRECENT)
                
                classifier.publish(name: self.pseudoLabel.text!, image: scaledImage) { (data: Data?, response: URLResponse?
                    , error: Error?) in
                    var ac:UIAlertController!
                    
                    self.activityView.stopAnimating()
                    
                    if let error = error {
                        ac = UIAlertController(title: "Publish error", message: error.localizedDescription, preferredStyle: .alert)
                    } else {
                        ac = UIAlertController(title: "Published!", message: "Your image has been published.", preferredStyle: .alert)
                    }
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                    
                }
            }
            
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        var ac:UIAlertController!
        if let error = error {
            ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
        } else {
            ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
        }
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        //let img = (info[.originalImage]  as? UIImage)!
        //let img2 = img1.fixOrientation()
        
        //let img3 = UIImage(cgImage: img2.cgImage!, scale: img2.scale, orientation:.left)

        
        takeImage.image = (info[.originalImage]  as? UIImage)!.fixOrientation()
    }
}
