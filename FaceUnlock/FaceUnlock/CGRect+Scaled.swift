//
//  CGRect+Scaled.swift
//  FaceUnlock
//
//  Created by LOUTFI, Nadir on 13/02/2019.
//  Copyright © 2019 LOUTFI, Nadir. All rights reserved.
//

import Foundation
import UIKit

extension CGRect {
    func scaled(to size: CGSize) -> CGRect {
        return CGRect(
            x: self.origin.x * size.width,
            y: self.origin.y * size.height,
            width: self.size.width * size.width,
            height: self.size.height * size.height
        )
    }
}
