//
//  FaceDectector.swift
//  FaceUnlock
//
//  Created by LOUTFI, Nadir on 13/02/2019.
//  Copyright © 2019 LOUTFI, Nadir. All rights reserved.
//

import UIKit
import CoreImage
import Vision


class FaceDetector: NSObject {
    fileprivate lazy var detector: CIDetector = {
        let context = CIContext()
        let options: [String : Any] = [
            CIDetectorAccuracy: CIDetectorAccuracyHigh,
            CIDetectorTracking: true
        ]
        let detector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options)!
        return detector
    }()
    
    fileprivate let featureDetectorOptions: [String : Any] = [
        CIDetectorImageOrientation: 6,   // Assuming portrait in this sample
    ]
 
    func detected(image: UIImage) -> Bool {
        return true
        let ciimage = CIImage(cgImage: image.cgImage!)
        let faces = detector.features(in: ciimage)
        if let face = faces.first as? CIFaceFeature {
            print("Found face at \(face.bounds)")

            if face.hasLeftEyePosition {
                print("Found left eye at \(face.leftEyePosition)")
            }
            
            if face.hasRightEyePosition {
                print("Found right eye at \(face.rightEyePosition)")
            }
            
            if face.hasMouthPosition {
                print("Found mouth at \(face.mouthPosition)")
            }
            return true
        }
        return false
    }
}
