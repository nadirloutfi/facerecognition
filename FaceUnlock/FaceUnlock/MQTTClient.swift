//
//  MQTTClient.swift
//  FaceUnlock
//
//  Created by LOUTFI, Nadir on 16/02/2019.
//  Copyright © 2019 LOUTFI, Nadir. All rights reserved.
//

import Foundation


import SwiftMQTT

// https://github.com/aciidb0mb3r/SwiftMQTT
class MQTTClient: NSObject {
    
    var mqttSession: MQTTSession!
    
    
    init(host: String) {
        super.init()
        mqttSession = MQTTSession(
            host: host,
            port: 1883,
            clientID: "swift", // must be unique to the client
            cleanSession: true,
            keepAlive: 15,
            useSSL: false
        )
        mqttSession.connect { error in
            if error == .none {
                print("Connected!")
            } else {
                print(error.description)
            }
        }
    }
    
    func publish(data: Data, topic: String) {
        mqttSession.publish(data, in: topic, delivering: .atLeastOnce, retain: false) { error in
            if error == .none {
                print("Published data in \(topic)!")
            } else {
                print(error.description)
            }
        }
    }
}
