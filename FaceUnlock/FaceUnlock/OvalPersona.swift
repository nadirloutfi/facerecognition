//
//  OvalPersona.swift
//  AV Foundation
//
//  Created by LOUTFI, Nadir on 14/02/2019.
//  Copyright © 2019 Pranjal Satija. All rights reserved.
//

import Foundation
import UIKit

class OvalPersona: UIView {
    
    override func draw(_ rect: CGRect) {
        let shapeLayer = CAShapeLayer()
        let width = rect.size.width
        let height = rect.size.height
        
        let ellipsePath = UIBezierPath(ovalIn: rect)
        shapeLayer.path = ellipsePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.lineWidth = 5.0
        self.layer.addSublayer(shapeLayer)
    }
    
}
